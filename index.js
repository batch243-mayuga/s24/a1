//exponent
const getCube = Math.pow(8, 3);
	console.log(`The cube of 8 is ${getCube}`);

// Array
const address = ["258 Washington Ave NW", "California 90011"];

const [city, state] = address;
console.log(`I live at ${city} , ${state}`);

// Object
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in"
}

let {name, species, weight, measurement} = animal;
	console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);

// forEach
const numbers = [1, 2, 3, 4, 5];

numbers.forEach((numbers)=>{
	console.log(numbers)
	
})

// reduce
const reduceNumber = numbers.reduce((x, y)=> x + y);
	console.log(reduceNumber)



// class
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog = new Dog("Mirmo", 3 , "Labrador");
console.log(dog);